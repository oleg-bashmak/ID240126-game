module VMCFG
	# ID задачи
	ID = "240126"

	# определяем подсеть
	SUBNET = "192.168.126."

	# проверять обновления репозитория?
	VM_BOX_CHECK_UPDATE = false

	# Использовать назначенные скрипты? Если =false, то скрипты в VM_CONFIG игнорируются
	SCRIPTS_USE = false

	# создаем список нужных виртуалок
	VM_CONFIG = [
	  { # имя ВМ
	    :vm_name => "game01",
	    # образ системы Ubuntu 
	    :box => "ubuntu/focal64",
	    # настройки ВМ
		:vbox_config => [
	        { "--cpus" => "1" },
	        { "--memory" => "2048" },
	        { "--description" => "Host Game01"}
	    ],

	    :script => [
	    	"./files/scripts/nginx.sh",
	    	"./files/scripts/git_clone.sh",
	    	"./files/scripts/node16.sh",
	    	"./files/scripts/game.sh"
	    ]
	  },
	  {
	   :vm_name => "game02",
	   :box => "ubuntu/focal64",
	   :vbox_config => [
	        { "--cpus" => "1" },
	        { "--memory" => "1024" },
	        { "--description" => "Host Game02"}
	    ]
	  },
	  {
	   :vm_name => "game03",
	   :box => "ubuntu/focal64",
	   :vbox_config => [
	        { "--cpus" => "1" },
	        { "--memory" => "1024" },
	        { "--description" => "Host Game03"}
	    ]
	  }

	]
end