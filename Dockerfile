FROM node:16.20.1 as BUILD

WORKDIR /app

COPY ./game-2048/package*.json .

RUN npm install --include=dev

COPY ./game-2048/ .

RUN npm run build


FROM node:16.20.1-alpine

WORKDIR /app_game2048

COPY --from=BUILD /app ./

CMD ["npm", "run", "start"]