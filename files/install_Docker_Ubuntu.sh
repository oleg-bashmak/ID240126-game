DOCKER_VERSION=25.0
wget -qO- https://get.docker.com/ | sed 's/lxc-docker/lxc-docker-ce-$DOCKER_VERSION/' | sh
sudo usermod -aG docker ${USER}